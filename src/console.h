#pragma once

#include "types.h"

void console_putc(char c);

void console_puts(const char* str);

void console_clear();