#include "console.h"
#include "asm.h"

#define NUM_COLS 80
#define NUM_ROWS 25

#define COLOR_BLACK 0
#define COLOR_WHITE 15

#define CHAR_BACKSPACE 0x08
#define CHAR_TAB 0x09
#define CHAR_SPACE 0x20

static uint16 cursorX;
static uint16 cursorY;
static uint16* videoMemory = (uint16*)0xB8000;

static uint16 char_with_color(char c, uint8 backColor, uint8 foreColor)
{
    return c | (((backColor << 4) | (foreColor & 0x0F)) << 8);
}

static void update_cursor()
{
    const uint16 cursorLocation = cursorY * NUM_COLS + cursorX;
    outb(0x3D4, 14);
    outb(0x3D5, cursorLocation >> 8);
    outb(0x3D4, 15);
    outb(0x3D5, cursorLocation);
}

static void scroll_if_needed()
{
    const uint16 blank = char_with_color(CHAR_SPACE, COLOR_BLACK, COLOR_WHITE);

    if (cursorY >= NUM_ROWS) {
        for (int i = 0; i < (NUM_ROWS - 1) * NUM_COLS; ++i)
            videoMemory[i] = videoMemory[i + NUM_COLS];

        for (int i = (NUM_ROWS - 1) * NUM_COLS; i < NUM_ROWS * NUM_COLS; ++i)
            videoMemory[i] = blank;

        cursorY = NUM_ROWS - 1;
    }
}

void console_putc(char c)
{
    if (c == CHAR_BACKSPACE) {
        --cursorX;
    } else if (c == CHAR_TAB) {
        cursorX = (cursorX + 8) & ~7;
    } else if (c == '\r') {
        cursorX = 0;
    } else if (c == '\n') {
        cursorX = 0;
        ++cursorY;
    } else if (c >= ' ') { // all printable characters
        uint16* location = videoMemory + (cursorY * NUM_COLS + cursorX);
        *location = char_with_color(c, COLOR_BLACK, COLOR_WHITE);
        ++cursorX;
    }

    if (cursorX >= NUM_COLS) {
        cursorX = 0;
        ++cursorY;
    }

    scroll_if_needed();
    update_cursor();
}

void console_puts(const char* str)
{
    int i = 0;
    while (str[i])
        console_putc(str[i++]);
}

void console_clear()
{
    const uint16 blank = char_with_color(CHAR_SPACE, COLOR_BLACK, COLOR_WHITE);

    for (int i = 0; i < NUM_COLS * NUM_ROWS; ++i)
        videoMemory[i]  = blank;

    cursorX = 0;
    cursorY = 0;

    update_cursor();
}
