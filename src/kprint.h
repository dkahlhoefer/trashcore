#pragma once

#include <stdarg.h>

/*
    Similar to standard printf.
    Only '%s' (string), '%i' (integer base 10) and '%x' (integer base 16) specifiers are supported.

    The minimum number of digits can be specified between 1 and 9 like so '%4x'.
    Default is 0 digits for base 10 and 8 digits for base 16.
*/
void kprint(const char* fmt, ...);

void kvprint(const char* fmt, va_list args);