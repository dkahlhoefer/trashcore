#include "console.h"
#include "kprint.h"
#include "x86/gdt.h"
#include "x86/idt.h"
#include "x86/timer.h"
#include "x86/paging.h"

int main(void* multibootHeader)
{
    console_clear();
    kprint("Welcome to TrashCore!\n");

    gdt_init();
    idt_init();
    paging_init();
    timer_init(100);

    uint32 *ptr = (uint32*)0xA0000000;
    uint32 do_page_fault = *ptr;

    asm volatile("sti");
    for (;;);

    kprint("Bye!\n");
    return 0xDEADBABA; 
}