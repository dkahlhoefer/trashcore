#include "idt.h"
#include "types.h"
#include "asm.h"
#include "memory.h"

typedef struct __attribute__((packed))
{
    uint16 base_low;
    uint16 selector;
    uint8 always0;
    uint8 flags;
    uint16 base_high;
} IdtEntry;

typedef struct __attribute__((packed))
{
    uint16 limit;
    uint32 base;
} IdtPtr;

#define NUM_IDT_ENTRIES 256

static IdtEntry idt_entries[NUM_IDT_ENTRIES];
static IdtPtr idt_ptr;

extern void idt_flush(uint32 idt_ptr);

extern void isr0();
extern void isr1();
extern void isr2();
extern void isr3();
extern void isr4();
extern void isr5();
extern void isr6();
extern void isr7();
extern void isr8();
extern void isr9();
extern void isr10();
extern void isr11();
extern void isr12();
extern void isr13();
extern void isr14();
extern void isr15();
extern void isr16();
extern void isr17();
extern void isr18();
extern void isr19();
extern void isr20();
extern void isr21();
extern void isr22();
extern void isr23();
extern void isr24();
extern void isr25();
extern void isr26();
extern void isr27();
extern void isr28();
extern void isr29();
extern void isr30();
extern void isr31();
extern void irq0();
extern void irq1();
extern void irq2();
extern void irq3();
extern void irq4();
extern void irq5();
extern void irq6();
extern void irq7();
extern void irq8();
extern void irq9();
extern void irq10();
extern void irq11();
extern void irq12();
extern void irq13();
extern void irq14();
extern void irq15();

static void idt_set_entry(uint8 index, uint32 base, uint16 selector, uint8 flags)
{
    idt_entries[index] = (IdtEntry){
        .base_low = base & 0xFFFF,
        .base_high = (base >> 16) & 0xFFFF,
        .selector = selector,
        .always0 = 0,
        .flags = flags /* | 0x60 */ // Uncomment the OR once we get to using user-mode (sets interupt gate privilege level to 3)
    };
}

void idt_init()
{
    // Remap the IRQ table
    outb(0x20, 0x11);
    outb(0xA0, 0x11);
    outb(0x21, 0x20);
    outb(0xA1, 0x28);
    outb(0x21, 0x04);
    outb(0xA1, 0x02);
    outb(0x21, 0x01);
    outb(0xA1, 0x01);
    outb(0x21, 0x0);
    outb(0xA1, 0x0);

    idt_ptr.limit = sizeof(IdtEntry) * NUM_IDT_ENTRIES - 1;
    idt_ptr.base = (uint32)&idt_entries;

    memset(&idt_entries, 0, sizeof(IdtEntry) * NUM_IDT_ENTRIES);

    idt_set_entry(0, (uint32)isr0, 0x08, 0x8E);
    idt_set_entry(1, (uint32)isr1, 0x08, 0x8E);
    idt_set_entry(2, (uint32)isr2, 0x08, 0x8E);
    idt_set_entry(3, (uint32)isr3, 0x08, 0x8E);
    idt_set_entry(4, (uint32)isr4, 0x08, 0x8E);
    idt_set_entry(5, (uint32)isr5, 0x08, 0x8E);
    idt_set_entry(6, (uint32)isr6, 0x08, 0x8E);
    idt_set_entry(7, (uint32)isr7, 0x08, 0x8E);
    idt_set_entry(8, (uint32)isr8, 0x08, 0x8E);
    idt_set_entry(9, (uint32)isr9, 0x08, 0x8E);
    idt_set_entry(10, (uint32)isr10, 0x08, 0x8E);
    idt_set_entry(11, (uint32)isr11, 0x08, 0x8E);
    idt_set_entry(12, (uint32)isr12, 0x08, 0x8E);
    idt_set_entry(13, (uint32)isr13, 0x08, 0x8E);
    idt_set_entry(14, (uint32)isr14, 0x08, 0x8E);
    idt_set_entry(15, (uint32)isr15, 0x08, 0x8E);
    idt_set_entry(16, (uint32)isr16, 0x08, 0x8E);
    idt_set_entry(17, (uint32)isr17, 0x08, 0x8E);
    idt_set_entry(18, (uint32)isr18, 0x08, 0x8E);
    idt_set_entry(19, (uint32)isr19, 0x08, 0x8E);
    idt_set_entry(20, (uint32)isr20, 0x08, 0x8E);
    idt_set_entry(21, (uint32)isr21, 0x08, 0x8E);
    idt_set_entry(22, (uint32)isr22, 0x08, 0x8E);
    idt_set_entry(23, (uint32)isr23, 0x08, 0x8E);
    idt_set_entry(24, (uint32)isr24, 0x08, 0x8E);
    idt_set_entry(25, (uint32)isr25, 0x08, 0x8E);
    idt_set_entry(26, (uint32)isr26, 0x08, 0x8E);
    idt_set_entry(27, (uint32)isr27, 0x08, 0x8E);
    idt_set_entry(28, (uint32)isr28, 0x08, 0x8E);
    idt_set_entry(29, (uint32)isr29, 0x08, 0x8E);
    idt_set_entry(30, (uint32)isr30, 0x08, 0x8E);
    idt_set_entry(31, (uint32)isr31, 0x08, 0x8E);
    idt_set_entry(32, (uint32)irq0, 0x08, 0x8E);
    idt_set_entry(33, (uint32)irq1, 0x08, 0x8E);
    idt_set_entry(34, (uint32)irq2, 0x08, 0x8E);
    idt_set_entry(35, (uint32)irq3, 0x08, 0x8E);
    idt_set_entry(36, (uint32)irq4, 0x08, 0x8E);
    idt_set_entry(37, (uint32)irq5, 0x08, 0x8E);
    idt_set_entry(38, (uint32)irq6, 0x08, 0x8E);
    idt_set_entry(39, (uint32)irq7, 0x08, 0x8E);
    idt_set_entry(40, (uint32)irq8, 0x08, 0x8E);
    idt_set_entry(41, (uint32)irq9, 0x08, 0x8E);
    idt_set_entry(42, (uint32)irq10, 0x08, 0x8E);
    idt_set_entry(43, (uint32)irq11, 0x08, 0x8E);
    idt_set_entry(44, (uint32)irq12, 0x08, 0x8E);
    idt_set_entry(45, (uint32)irq13, 0x08, 0x8E);
    idt_set_entry(46, (uint32)irq14, 0x08, 0x8E);
    idt_set_entry(47, (uint32)irq15, 0x08, 0x8E);

    idt_flush((uint32)&idt_ptr);
}