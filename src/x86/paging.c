#include "paging.h"
#include "kmalloc.h"
#include "memory.h"
#include "panic.h"
#include "kprint.h"
#include "isr.h"
#include "bitset.h"

static Bitset frames;

static PageDirectory* kernel_directory = 0;
static PageDirectory* current_directory = 0;

// defined in kmalloc.c
extern uintptr placement_address;

#define INDEX_FROM_BIT(a) (a/(8*4))
#define OFFSET_FROM_BIT(a) (a%(8*4))

// Set a bit in the frames bitset
static void set_frame(uintptr frame_address)
{
    bitset_set(&frames, frame_address / 0x1000);
}

// Clear a bit in the frames bitset
static void clear_frame(uintptr frame_address)
{
    bitset_clear(&frames, frame_address / 0x1000);
}

// Test if a bit is set in the frames bitset
static bool test_Frame(uintptr frame_address)
{
    return bitset_test(&frames, frame_address / 0x1000);
}

static uint32 find_first_free_frame()
{
    return bitset_find_first_clear(&frames);
}

static void alloc_frame(Page* page, bool is_kernel, bool is_writable)
{
    // If frame was already allocated, do nothing
    if (page->frame != 0)
        return;

    const uint32 index = find_first_free_frame();

    if (index == (uint32)-1)
        panic("No free page frames");

    set_frame(index * 0x1000);
    page->present = true;
    page->read_write = !is_writable;
    page->user = !is_kernel;
    page->frame = index;
}

static void free_frame(Page* page)
{
    // If frame is not allocated, do nothing
    if (page->frame == 0)
        return;

    clear_frame(page->frame);
    page->frame = 0;
}

static void handle_page_fault(Registers regs)
{
    uintptr faulting_address;
    asm volatile("mov %%cr2, %0" : "=r"(faulting_address));

    const bool present = !(regs.error_code & 0x1);
    const bool read_only = regs.error_code & 0x2;
    const bool user_mode = regs.error_code & 0x4;
    const bool reserved = regs.error_code & 0x8; // CPU-reserved bits of page entry were overwritten
    const bool instruction_fetch = regs.error_code & 0x10;

    kprint("\n==== Page fault ====\nerror codes:");
    if (present) kprint(" present");
    if (read_only) kprint(" read-only");
    if (user_mode) kprint(" user-mode");
    if (reserved) kprint(" reserved");
    if (instruction_fetch) kprint(" instruction-fetch");
    kprint("\nat %x\n", faulting_address);

    panic("Page fault");
}

void paging_init()
{
    // Size of physical memory. Assume 16 MB for the moment.
    const uintptr mem_end_page = 0x1000000;

    frames = bitset_create(mem_end_page / 0x1000, kmalloc);
    
    kernel_directory = (PageDirectory*)kmalloc_a(sizeof(PageDirectory));
    memset(kernel_directory, 0, sizeof(PageDirectory));
    current_directory = kernel_directory;

    // Identity map (physical address = virtual address) all the memory
    uintptr address = 0;
    while (address < placement_address) {
        // Kernel code is readable but not writable from userspace
        alloc_frame(paging_get_page(address, true, kernel_directory), false, false);
        address += 0x1000;
    }

    register_interrupt_handler(INT_PAGE_FAULT, handle_page_fault);

    paging_switch_directory(kernel_directory);
}

void paging_switch_directory(PageDirectory* dir_to_use)
{
    current_directory = dir_to_use;
    asm volatile("mov %0, %%cr3" : : "r"(&dir_to_use->tables_physical));
    uint32 cr0;
    asm volatile("mov %%cr0, %0" : "=r"(cr0));
    cr0 |= 0x80000000; // Enable paging
    asm volatile("mov %0, %%cr0" : : "r"(cr0));
}

Page* paging_get_page(uintptr address, bool create_table_if_not_exist, PageDirectory* dir)
{
    const uint32 index = address / 0x1000;
    const uint32 table_index = index / 1024;

    if (dir->tables[table_index] != NULL)
        return &dir->tables[table_index]->pages[index % 1024]; // table is already assigned

    if (!create_table_if_not_exist)
        return NULL;
    
    uint32 temp;
    dir->tables[table_index] = (PageTable*)kmalloc_ap(sizeof(PageTable), &temp);
    memset(dir->tables[table_index], 0, sizeof(PageTable));
    dir->tables_physical[table_index] = temp | 0x7; // present, read_write, user
    return &dir->tables[table_index]->pages[index % 1024];
}