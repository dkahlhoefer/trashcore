#pragma once

#include "types.h"

enum
{
    INT_DIV_BY_ZERO = 0,
    INT_DEBUG_EXCEPTION = 1,
    INT_NON_MASKABLE = 2,
    INT_BREAKPOINT = 3,
    INT_OVERFLOW = 4,
    INT_OUT_OF_BOUNDS = 5,
    INT_INVALID_OPCODE = 6,
    INT_NO_COPROCESSOR = 7,
    INT_DOUBLE_FAULT = 8,
    INT_COPROCESSOR_SEGMENT_OVERRUN = 9,
    INT_INVALID_TSS = 10,
    INT_SEGMENT_NOT_PRESENT = 11,
    INT_STACK_SEGMENT_FAULT = 12,
    INT_GENERAL_PROTECTION = 13,
    INT_PAGE_FAULT = 14,
    INT_FLOATING_POINT_ERROR = 16,
    INT_ALIGNMENT_CHECK = 17,
    INT_MACHINE_CHECK = 18,
    IRQ0 = 32,
    IRQ1 = 33,
    IRQ2 = 34,
    IRQ3 = 35,
    IRQ4 = 36,
    IRQ5 = 37,
    IRQ6 = 38,
    IRQ7 = 39,
    IRQ8 = 40,
    IRQ9 = 41,
    IRQ10 = 42,
    IRQ11 = 43,
    IRQ12 = 44,
    IRQ13 = 45,
    IRQ14 = 46,
    IRQ15 = 47
};

typedef struct
{
    uint32 ds;
    uint32 edi, esi, ebp, esp, ebx, edx, ecx, eax;
    uint32 interrupt_number, error_code;
    uint32 eip, cs, eflags, user_esp, ss;
} Registers;

typedef void (*InterruptHandler)(Registers);

void register_interrupt_handler(uint8 interrupt_number, InterruptHandler handler);