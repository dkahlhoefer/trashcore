#include "kmalloc.h"

extern uint32 kernel_end;
uintptr placement_address = (uintptr)&kernel_end;

static void* kmalloc_internal(uint32 size, bool page_align, uintptr* physical_address)
{
    // First page-align placement_address if desired
    if (page_align && (placement_address & 0xFFFFF000)) {
        placement_address &= 0xFFFFF000;
        placement_address += 0x1000;
    }

    if (physical_address != NULL)
        *physical_address = placement_address;

    uintptr tmp = placement_address;
    placement_address += size;
    return (void*)tmp;
}

// Allocate page-aligned memory
void* kmalloc_a(uint32 size)
{
    return kmalloc_internal(size, true, NULL);
}

// Allocate memory and also return the physical address
void* kmalloc_p(uint32 size, uintptr* physical_address)
{
    return kmalloc_internal(size, false, physical_address);
}

// Allocate page-aligned memory and also return the physical address
void* kmalloc_ap(uint32 size, uintptr* physical_address)
{
    return kmalloc_internal(size, true, physical_address);
}

// Allocate memory
void* kmalloc(uint32 size)
{
    return kmalloc_internal(size, false, NULL);
}