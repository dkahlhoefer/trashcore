#include "gdt.h"
#include "types.h"

typedef struct __attribute__((packed))
{
    uint16 limit_low;
    uint16 base_low;
    uint8 base_middle;
    uint8 access;
    uint8 granularity;
    uint8 base_high;
} GdtEntry;

typedef struct __attribute__((packed)) 
{
    uint16 limit;
    uint32 base;
} GdtPtr;

#define NUM_GDT_ENTRIES 5

static GdtEntry gdt_entries[NUM_GDT_ENTRIES];
static GdtPtr gdt_ptr;

extern void gdt_flush(uint32 gdt_ptr);

static void gdt_set_entry(uint8 index, uint32 base, uint32 limit, uint8 access, uint8 granularity)
{
    gdt_entries[index] = (GdtEntry){
        .base_low = (base & 0xFFFF),
        .base_middle = (base >> 16) & 0xFF,
        .base_high = (base >> 24) & 0xFF,
        .limit_low = (limit & 0xFFFF),
        .granularity = ((limit >>  16) & 0x0F) | (granularity & 0xF0),
        .access = access
    };
}

void gdt_init()
{
    gdt_ptr.limit = sizeof(GdtEntry) * NUM_GDT_ENTRIES - 1;
    gdt_ptr.base = (uint32)&gdt_entries;

    // Setup a flat memory model
    gdt_set_entry(0, 0, 0, 0, 0); // Null segment
    gdt_set_entry(1, 0, 0xFFFFFFFF, 0x9A, 0xCF); // Code segment
    gdt_set_entry(2, 0, 0xFFFFFFFF, 0x92, 0xCF); // Data segment
    gdt_set_entry(3, 0, 0xFFFFFFFF, 0xFA, 0xCF); // User mode code segment
    gdt_set_entry(4, 0, 0xFFFFFFFF, 0xF2, 0xCF); // User mode data segment

    gdt_flush((uint32)&gdt_ptr);
}