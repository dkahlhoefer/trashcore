#pragma once

#include "types.h"

typedef struct __attribute__((packed))
{
    bool present : 1;
    bool read_write : 1;
    bool user : 1;
    bool accessed : 1;
    bool dirty : 1;
    uint8 unused : 7;
    uint32 frame : 20; // Frame address, shifted right 12 bits
} Page;

typedef struct __attribute__((packed))
{
    Page pages[1024];
} PageTable;

typedef struct __attribute__((packed))
{
    PageTable* tables[1024];
    uintptr tables_physical[1024];
    uintptr physical_address;
} PageDirectory;

void paging_init();

void paging_switch_directory(PageDirectory* dir_to_use);

Page* paging_get_page(uintptr address, bool create_table_if_not_exist, PageDirectory* dir);