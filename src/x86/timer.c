#include "timer.h"
#include "isr.h"
#include "asm.h"

uint32 tick = 0;

static void timer_callback(Registers regs)
{
    (void)(regs);
    
    ++tick;
}

void timer_init(uint32 frequency_hz)
{
    register_interrupt_handler(IRQ0, timer_callback);

    const uint32 divisor = 1193180 / frequency_hz;

    outb(0x43, 0x36);

    const uint8 l = (uint8)(divisor & 0xFF);
    const uint8 h = (uint8)((divisor >> 8) & 0xFF);
    outb(0x40, l);
    outb(0x40, h);
}