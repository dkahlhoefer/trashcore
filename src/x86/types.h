#pragma once

typedef unsigned int uint32;
typedef int int32;
typedef unsigned short uint16;
typedef short int16;
typedef unsigned char uint8;
typedef char int8;

typedef uint32 size_t;
typedef uint32 uintptr;

typedef _Bool bool;
enum { false, true };

#define NULL 0