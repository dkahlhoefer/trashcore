#include "isr.h"
#include "asm.h"
#include "kprint.h"

InterruptHandler interrupt_handlers[256];

void register_interrupt_handler(uint8 interrupt_number, InterruptHandler handler)
{
    interrupt_handlers[interrupt_number] = handler;
}

void isr_handler(Registers registers)
{
    if (interrupt_handlers[registers.interrupt_number] != NULL)
        interrupt_handlers[registers.interrupt_number](registers);
}

void irq_handler(Registers registers)
{
    if (registers.interrupt_number >= 40)
        outb(0xA0, 0x20); // Send reset signal to slave PIC

    outb(0x20, 0x20); // Send reset signal to master PIC

    if (interrupt_handlers[registers.interrupt_number] != NULL)
        interrupt_handlers[registers.interrupt_number](registers);
}