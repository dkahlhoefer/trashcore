#include "panic.h"
#include "assert.h"
#include "kprint.h"

void panic_impl(const char* file, int line, const char* message, ...)
{
    asm volatile("cli");

    va_list args;
    va_start(args, message);

    kprint("\n==== Kernel panic! ====\n");
    kprint("in '%s', line %i\n\n", file, line);
    kvprint(message, args);
    kprint("\n");

    asm volatile("hlt");
}

void assert_impl(bool condition, const char* file, int line, const char* message)
{
    if (!condition)
        panic_impl(file, line, message);
}