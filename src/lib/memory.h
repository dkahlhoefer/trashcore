#pragma once

#include "types.h"

void* memcpy(void* dest, const void* src, size_t count);

void* memset(void* dest, uint8 ch, size_t count);