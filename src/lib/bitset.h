#pragma once

#include "types.h"

typedef struct
{
    uint32* bits;
    uint32 num_bits;
} Bitset;

Bitset bitset_create(uint32 num_bits, void* (*malloc_func) (uint32));

void bitset_destroy(Bitset* bitset, void (*free_func)(void*));

void bitset_set(Bitset* bitset, uint32 bit_index);

void bitset_clear(Bitset* bitset, uint32 bit_index);

bool bitset_test(Bitset* bitset, uint32 bit_index);

uint32 bitset_find_first_clear(Bitset* bitset);