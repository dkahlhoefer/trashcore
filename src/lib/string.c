#include "string.h"

char* strcpy(char* dest, const char* src)
{
    char* ret = dest;

    do {
        *(dest++) = *(src++);
    } while (*src != '\0');

    return ret;
}

int strcmp(const char* lhs, const char* rhs)
{
    const unsigned char* l = (const unsigned char*)lhs;
    const unsigned char* r = (const unsigned char*)rhs;

    while (*l != '\0' && *l == *r) {
        ++l;
        ++r;
    }

    return (*l > *r) - (*r > *l);
}

size_t strlen (const char* str)
{
    size_t length = 0;

    while (*(str++) != '\0')
        ++length;

    return length;
}