#include "memory.h"

void* memcpy(void* dest, const void* src, size_t count)
{
    const uint8* srcByte = src;
    uint8* destByte = dest;

    while (count-- > 0)
        *(destByte++) = *(srcByte++);

    return dest;
}

void* memset(void* dest, uint8 ch, size_t count)
{
    uint8* destByte = dest;

    while (count-- > 0)
        *(destByte++) = ch;

    return dest;
}