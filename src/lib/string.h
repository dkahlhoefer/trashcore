#pragma once

#include "types.h"

char* strcpy(char* dest, const char* src);

int strcmp(const char* lhs, const char* rhs);

size_t strlen(const char* str);