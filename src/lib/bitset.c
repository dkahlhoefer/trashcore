#include "bitset.h"
#include "memory.h"

#define INDEX_FROM_BIT(a) (a/(8*4))
#define OFFSET_FROM_BIT(a) (a%(8*4))

Bitset bitset_create(uint32 num_bits, void* (*malloc_func) (uint32))
{
    Bitset bitset = {
        .num_bits = num_bits,
        .bits = (uint32*)malloc_func(INDEX_FROM_BIT(num_bits) * sizeof(*bitset.bits))
    };

    memset(bitset.bits, 0, INDEX_FROM_BIT(num_bits) * sizeof(*bitset.bits));
    return bitset;
}

void bitset_destroy(Bitset* bitset, void (*free_func)(void*))
{
    free_func (bitset->bits);
    bitset->num_bits = 0;
}

void bitset_set(Bitset* bitset, uint32 bit_index)
{
    const uint32 index = INDEX_FROM_BIT(bit_index);
    const uint32 offset = OFFSET_FROM_BIT(bit_index);
    bitset->bits[index] |= (1 << offset);
}

void bitset_clear(Bitset* bitset, uint32 bit_index)
{
    const uint32 index = INDEX_FROM_BIT(bit_index);
    const uint32 offset = OFFSET_FROM_BIT(bit_index);
    bitset->bits[index] &= ~(1 << offset);
}

bool bitset_test(Bitset* bitset, uint32 bit_index)
{
    const uint32 index = INDEX_FROM_BIT(bit_index);
    const uint32 offset = OFFSET_FROM_BIT(bit_index);
    return bitset->bits[index] & (1 << offset);
}

uint32 bitset_find_first_clear(Bitset* bitset)
{
    for (uint32 index = 0; index < INDEX_FROM_BIT(bitset->num_bits); ++index) {
        if (bitset->bits[index] != 0xFFFFFFFF) { // is there at least one clear bit?
            for (uint32 offset = 0; offset < 32; ++offset) {
                if (!(bitset->bits[index] & (1 << offset)))
                    return index * 4 * 8 + offset;
            }
        }
    }

    return (uint32)-1;
}