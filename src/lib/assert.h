#pragma once

#include "types.h"

#define assert(condition) assert_impl((condition), __FILE__, __LINE__, "Assertion failed: " #condition);

void assert_impl(bool condition, const char* file, int line, const char* message);
