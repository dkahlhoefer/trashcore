#pragma once

#include "types.h"

typedef bool(*LessThanPredicate)(uint32, uint32);

typedef struct
{
    uint32* values;
    uint32 size;
    uint32 max_size;
    LessThanPredicate less_than;
} SortedArray;

bool sorted_array_default_less_than(uint32 a, uint32 b);

SortedArray sorted_array_create(uint32 max_size, LessThanPredicate less_than);

void sorted_array_destroy(SortedArray* array);

void sorted_array_insert(SortedArray* array, uint32 value);

uint32 sorted_array_loopup(SortedArray* array, uint32 index);

void sorted_array_remove(SortedArray* array, uint32 index);