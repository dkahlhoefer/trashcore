#include "sorted_array.h"
#include "kmalloc.h"
#include "memory.h"
#include "assert.h"

bool sorted_array_default_less_than(uint32 a, uint32 b)
{
    return a < b;
}

// TODO pass malloc function pointer
SortedArray sorted_array_create(uint32 max_size, LessThanPredicate less_than)
{
    assert(less_than != NULL);

    SortedArray array = {
        .values = kmalloc(max_size * sizeof(*array.values)),
        .size = 0,
        .max_size = max_size,
        .less_than = less_than
    };
    
    memset(array.values, 0, max_size * sizeof(*array.values));
    
    return array;
}

void sorted_array_destroy(SortedArray* array)
{
    (void)(array);
    // kfree(array-values);
}

void sorted_array_insert(SortedArray* array, uint32 value)
{
    assert(array->size < array->max_size);

    uint32 i = 0;
    while (i < array->size && array->less_than(array->values[i], value))
        ++i;

    if (i == array->size) { // just add at the end of the array
        array->values[array->size++] = value;
    } else {
        uint32 tmp_value = array->values[i];
        array->values[i] = value;

        while (i < array->size) {
            ++i;
            const uint32 tmp_value2 = array->values[i];
            array->values[i] = tmp_value;
            tmp_value = tmp_value2;
        }

        array->size++;
    }
}

uint32 sorted_array_loopup(SortedArray* array, uint32 index)
{
    assert(index < array->size);
    return array->values[index];
}

void sorted_array_remove(SortedArray* array, uint32 index)
{
    assert(index < array->size);
    
    while (index < array->size) {
        array->values[index] = array->values[index + 1];
        ++index;
    }

    array->size--;
}