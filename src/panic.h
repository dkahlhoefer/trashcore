#pragma once

#define panic(message, ...) panic_impl(__FILE__, __LINE__, message __VA_OPT__(,) __VA_ARGS__)

void panic_impl(const char* file, int line, const char* message, ...);