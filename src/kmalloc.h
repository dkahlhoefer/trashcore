#pragma once

#include "types.h"

// Allocate page-aligned memory
void* kmalloc_a(uint32 size);

// Allocate memory and also return the physical address
void* kmalloc_p(uint32 size, uintptr* physical_address);

// Allocate page-aligned memory and also return the physical address
void* kmalloc_ap(uint32 size, uintptr* physical_address);

// Allocate memory
void* kmalloc(uint32 size);