#include "kprint.h"
#include "console.h"
#include "types.h"

static const char digits[] = "0123456789ABCDEF";

static void kprint_int32_10(int32 i, uint8 num_digits)
{
    if (i == 0) {
        num_digits = num_digits == 0 ? 1 : num_digits;
        while (num_digits-- > 0)
            console_putc('0');

        return;
    }

    if (i < 0) {
        console_putc('-');
        i *= -1;
    }

    int32 divisor = 1000000000;

    uint8 zeros = 10;
    while (divisor > i) {
        divisor /= 10;

        if (zeros-- <= num_digits)
            console_putc('0');
    }

    while (divisor > 0) {
        const int32 d = i / divisor;
        console_putc(digits[d]);

        i -= d * divisor;
        divisor /= 10;
    }
}

static void kprint_uint32_16(uint32 i, uint8 num_digits)
{
    console_putc('0');
    console_putc('x');
    if (i == 0) {
        while (num_digits-- > 0)
            console_putc('0');

        return;
    }

    uint32 divisor = 0x10000000;

    uint8 zeros = 8;
    while (divisor > i) {
        divisor /= 0x10;

        if (zeros-- <= num_digits)
            console_putc('0');
    }

    while (divisor > 0) {
        const uint32 d = i / divisor;
        console_putc(digits[d]);

        i -= d * divisor;
        divisor /= 0x10;
    }
}

void kvprint(const char* fmt, va_list args)
{
    bool await_specifier = false;
    uint8 num_digits = 0;

    while (*fmt != '\0') {
        const char c = *fmt++;
        if (await_specifier) {
            if (c >= '1' && c <= '9') {
                num_digits = c - 0x30;
            } else {
                if (c == 'i') {
                    kprint_int32_10(va_arg(args, int32), num_digits);
                } else if (c == 'x') {
                    kprint_uint32_16(va_arg(args, uint32), num_digits == 0 ? 8 : num_digits);
                } else if (c == 's') {
                    console_puts(va_arg(args, char*));
                }
                await_specifier = false;
            }
        } else {
            if (c == '%')
                await_specifier = true;
            else
                console_putc(c);
        }
    }
}

void kprint(const char* fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    kvprint(fmt, args);
}
